import axios from 'axios';

let axiosDefaults = require('axios/lib/defaults');
axiosDefaults.baseURL = 'http://localhost:8000';

export default {

    characters: {
        fetchAll: () => axios.get("/api/character").then(res => res.data),
        create: character =>
            axios.post("/api/character", { character }).then(res => res.data)
    }
};