import {
    SET_LIKES,
    CHARACTERS_REQUEST,
    CHARACTERS_SUCCESS,
    CHARACTERS_ERROR
} from './types/character';

export const fetchCharacterSaga = () => ({
    type: CHARACTERS_REQUEST,
})

export const fetchCharacterSagaSuccess = payload => ({
    type: CHARACTERS_SUCCESS,
    payload
})

export const fetchCharacterSagaError = payload => ({
    type: CHARACTERS_ERROR,
    payload
})

export const setLikes = payload => ({
 type: SET_LIKES,
 payload,
});

export const increaseLikes = () => (dispatch, getState) => {
    const {likes} = getState().character;
    const totalLikes = likes + 1;
    dispatch(setLikes(totalLikes))
};

export const decreaseLikes = () => (dispatch, getState) => {
    const {likes} = getState().character;
    const totalLikes = likes - 1;
    const totalLikesFinal = totalLikes<0 ? 0 :totalLikes;
    dispatch(setLikes(totalLikesFinal))
}

export const resetLikes = () => (dispatch, getState) => {
    dispatch(setLikes(0))
}