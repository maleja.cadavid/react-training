import {takeLatest} from 'redux-saga/effects';
import {fetchCharacterSaga} from './characterSaga';
import { CHARACTERS_REQUEST} from '../actions/types/character';

export default function* rootSaga() {
    yield takeLatest(CHARACTERS_REQUEST, fetchCharacterSaga)
}