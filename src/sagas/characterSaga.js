import { call, put} from 'redux-saga/effects';
import {
    fetchCharacterSagaSuccess,
    fetchCharacterSagaError,
} from '../actions/character';
import api from '../api';

export function* fetchCharacterSaga(action) {
    try {
        const characters = yield call(
            api.characters.fetchAll
        )
        yield put(fetchCharacterSagaSuccess(characters))
    } catch (err) {
        yield put(fetchCharacterSagaError(err.response))
    }
}