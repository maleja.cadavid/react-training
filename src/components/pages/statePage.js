import React from 'react';
import { Button, Segment} from "semantic-ui-react";
import { connect } from 'react-redux';
import { increaseLikes } from "../../actions/character";

class stateLocal extends React.Component {
    state = {}

    render() {
        return (
            <Segment>
                <h1>Local state</h1>
                <Button onClick={this.props.increaseLikes}
                    color='red'
                    content='Like'
                    icon='heart'
                    label={{basic: true, color: 'red', pointing: 'left', content: this.props.likes}}
                />
            </Segment>
        );
    }
}

const mapStateProps = (state) => ({
    likes: state.character.likes
});

export default connect(mapStateProps, {increaseLikes})(stateLocal);