import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Dimmer, Loader} from "semantic-ui-react";
import { increaseLikes, decreaseLikes, resetLikes, fetchCharacterSaga} from "../../actions/character";
import List from '../common/list';

class Dashboard extends React.Component{
    state = {}

    componentDidMount() {
        this.props.fetchCharacterSaga()
    }

    render() {
        const { character } =  this.props;

        if(character.fetching) { return (<Dimmer active><Loader>Loading</Loader></Dimmer>)}
        return (
            <div>
                <h1> Dashboard likes {this.props.likes}</h1>
                <button onClick={this.props.increaseLikes}> increase like</button>
                <br/>
                <button onClick={this.props.decreaseLikes}> decrease like</button>
                <br/>
                <button onClick={this.props.resetLikes}> reset likes</button>
                <br/>
                <Link to={"/detail"}> go to detail page</Link>
                /
                <Link to={"/state"}> go to state page</Link>
                <br/>
                <h3>likes from redux : {this.props.likes}</h3>
                <br/>
                <h1>Characters list</h1>
                {character.characters.length?(<List characters={character.characters}/>):( <h3>Empty list</h3>)}
            </div>
        );
    }

}

const mapStateProps = (state) => ({
    likes: state.character.likes,
    character: state.character
});

export default connect(mapStateProps, {increaseLikes, decreaseLikes, resetLikes, fetchCharacterSaga})(Dashboard);